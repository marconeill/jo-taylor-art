<?php get_header(); ?>

<?php if (is_front_page() ) { ?>

  <div id="kennylad">

  <ul class="slideshow">
    <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/image1a.jpg" /></li>
<!--    <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/image1b.jpg" /></li> -->
<!--      <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/jotaylor-img1.jpg" /></li> -->
 <!--     <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/jotaylor-img2.jpg" /></li> -->
  <!--    <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/jotaylor-img3.jpg" /></li> -->
   <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/1.jpg" /></li> 
    <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/2.jpg" /></li> 
    <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/3.jpg" /></li> 
    <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/Autumn-Exhibitions-111.jpg" /></li> 
    <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/After-The-Fire.jpg" /></li> 
 <!--  <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/The-Optimist-016.jpg" /></li> -->
    <!--  <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/Spring-Work-I-046.jpg" /></li> -->
     <li><img src="<?php echo get_template_directory_uri(); ?>/theme/img/slide/Spring-Work-I-005.jpg" /></li> 
  </ul>

  </div>

<?php } else { ?>

<?php } ?>

<script>
  var slideshow = new KenBurnsSlideshow({
    el: document.querySelector('.slideshow')
  })
  slideshow.init()
</script>



<div class="container-responsive mt-5">

  <div class="row">

    <div class="col">

      <div id="content" role="main">

        <?php get_template_part('loops/page-content'); ?>
      </div><!-- /#content -->
    </div>

  </div><!-- /.row -->
</div><!-- /.container-responsive -->

<?php get_footer(); ?>
