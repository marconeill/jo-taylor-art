<?php
/**!
 * The Page Content Loop
 */
?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
  <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>


    <?php
    if ( function_exists('yoast_breadcrumb') ) {
    yoast_breadcrumb('
    <p id="breadcrumbs">','</p>
    ');
    }
    ?>


    <header class="mb-4 border-bottom">
      <h1>
        <?php the_title()?>
      </h1>
    </header>
    <main>

      <?php the_content()?>


      <?php
      $args = array(
          'parent' => $post->ID,
          'post_type' => 'page',
          'post_status' => 'publish'
      );
      $pages = get_pages($args);  ?>
      <div class="row justify-content-center">
      <?php foreach( $pages as $page ) { ?>
       <div class="col-sm-4 text-center mb-4">
          <div><a href="<?php echo  get_permalink($page->ID); ?>" rel="bookmark" title="<?php echo $page->post_title; ?>">
            <?php echo get_the_post_thumbnail($page->ID, 'thumbnail'); ?>
          </a></div>
         <div> <a href="<?php echo  get_permalink($page->ID); ?>" rel="bookmark" title="<?php echo $page->post_title; ?>">
          <span class="title"><?php echo $page->post_title; ?></span>
          </a></div>
      </div>


      <?php } ?>
</div>



      <?php wp_link_pages(); ?>
    </main>
  </article>
<?php
  endwhile; else:
    wp_redirect(esc_url( home_url() ) . '/404', 404);
    exit;
  endif;
?>
