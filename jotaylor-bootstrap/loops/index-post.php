<?php
/*
The Index Post (or excerpt)
===========================
Used by index.php, category.php and author.php
*/
?>

<article class="mb-4" role="article" id="post_<?php the_ID()?>" <?php post_class(); ?> >
  <header>
    <?php the_post_thumbnail('medium'); ?>
    <h2 class="mr-md-4 pl-md-2 pr-md-1">
      <a href="<?php the_permalink(); ?>">
        <?php the_title()?>
      </a>
    </h2>
    <p class="text-muted pl-md-2">
      <!--<i class="far fa-calendar-alt"></i>&nbsp;<?php b4st_post_date(); ?>&nbsp;|-->
      <i class="far fa-user"></i>&nbsp; <?php _e('By ', 'b4st'); ?> Jo
    </p>

  </header>
  <hr class="mr-md-5"></hr>

  <!--<main>
    <?php if ( has_excerpt( $post->ID ) ) {
  		the_excerpt();
    ?><p><a href="<?php the_permalink(); ?>">
    	<?php _e( '&hellip; ' . __('Continue reading', 'b4st' ) . ' <i class="fas fa-arrow-right"></i>', 'b4st' ) ?>
      </a></p>
  	<?php } else {
  		the_content( __( '&hellip; ' . __('Continue reading', 'b4st' ) . ' <i class="fas fa-arrow-right"></i>', 'b4st' ) );
		} ?>
  </main>-->
</article>
