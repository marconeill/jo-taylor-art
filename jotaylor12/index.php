<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div id="content-m"><article <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<h1><?php the_title(); ?></a></h1>

		<!--<?php include (TEMPLATEPATH . '/_/inc/meta.php' ); ?>-->

			<div class="entry">
				<?php the_content(); ?>
			</div>

			

		</article></div>

	<?php endwhile; ?>

	<?php include (TEMPLATEPATH . '/_/inc/nav.php' ); ?>

	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>

<!--<?php get_sidebar(); ?>-->

<?php get_footer(); ?>
