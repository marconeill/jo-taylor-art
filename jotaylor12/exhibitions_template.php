<?php
/**
 * Template Name: Page of Exhibitions
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_header(); ?>

<div id="content-m">

<?php
$page_id = 9; // substitute page_id of page you want content included from for "2"
$page = get_post($page_id);
echo "<h1>$page->post_title</h1>";
echo "<p>$page->post_content</p>";
?>

<?php
$type = 'Exhibitions'; /** Gets all exhibitions posts */
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'caller_get_posts'=> 1
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post(); ?>
<!-- Individual Address Block Starts -->
<div id="address-block-ex"> 
	
	<h2><?php the_title(); ?></h2>
	<p>
	<?php echo get('address_address_1'); ?>,<br />

<!-- Checks if item has a value, if it doesn't it returns blank rather than <br> -->	
<?php 
$address2 = get(address_address_2);

if ($address == NULL) {echo "";} 
else
{echo get('address_address_2'),'<br>';}
?>		
	
	<?php echo get('address_town_city'); ?>,<br />

<?php 
$county = get(address_county);

if ($county == NULL) {echo "";} 
else
{echo get('address_county'),'<br>';}
?>		
	
	<?php echo get('address_post_code'); ?><br />

<?php 
$country = get(address_country);

if ($country == NULL) {echo "";} 
else
{echo get('address_country'),'<br>';}
?>	

	<?php echo get('webemail_contact_1'); ?><br />
	
<?php 
$web2 = get(webemail_contact_2);

if ($web2 == NULL) {echo "";} 
else
{echo get('webemail_contact_2'),'<br>';}
?>
	
	<a href="mailto:<?php echo get('webemail_email_address'); ?>" target="_blank"><?php echo get('webemail_email_address'); ?></a><br />
	<a href="http://<?php echo get('webemail_website_url'); ?>" target="_blank"><?php echo get('webemail_website_url'); ?></a>
	</p></div>


    <?php
  endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>
            </div><!-- #content -->


<?php get_footer(); ?>