<!DOCTYPE html>

<!--[if lt IE 7 ]> <html class="ie ie6 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<head>

	<meta charset="<?php bloginfo('charset'); ?>">

	<meta name="viewport" content="width=device-width" />

	<?php if (is_search()) { ?>
	<meta name="robots" content="noindex, nofollow" />
	<?php } ?>

	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		      wp_title(''); echo '  '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_front_page()) {
		         echo 'Jo Taylor - Artist - Contemporary Equestrian Art, Equestrian Paintings, Horse Paintings, Horse Art'; }
		      else {
		          ; }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
	<link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

	<meta name="description" content="<?php bloginfo('description'); ?>">

	<meta name="google-site-verification" content="">

	<meta name="author" content="Marc O'Neill">

	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/_/img/favicon.ico">

	<link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/_/img/apple-touch-icon.png">

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

	<script src="<?php bloginfo('template_directory'); ?>/_/js/modernizr-1.7.min.js"></script>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>

	<?php if (is_front_page()  ) {?>

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
		<script type="text/javascript" src="<?php get_site_url(); ?>wp-includes/js/kenburns.js"></script>
		<script type="text/javascript">
			$(function(){
				$('#kenburns').kenburns({
					images:[
						'images/image1a.jpg',
						'images/image1b.jpg',
						'images/jotaylor-img1.jpg',
            'images/jotaylor-img2.jpg',
            'images/jotaylor-img3.jpg',
						'images/1.jpg',
            'images/2.jpg',
            'images/3.jpg',
						'images/Autumn-Exhibitions-111.jpg',
						'images/After-The-Fire.jpg',
						'images/The-Optimist-016.jpg',
						'images/Spring-Work-I-046.jpg',
						'images/Spring-Work-I-005.jpg'
					     	],
					frames_per_second: 30,
					display_time: 20000,
					fade_time: 15000,
					zoom: 2,
					background_color:'#ffffff',
					post_render_callback:function($canvas, context) {
						// Called after the effect is rendered
						// Draw anything you like on to of the canvas

						context.save();
						context.fillStyle = '#000';
						context.font = 'bold 20px sans-serif';
						var width = $canvas.width();
						var height = $canvas.height();px
						var text = "";
						var metric = context.measureText(text);

						context.fillStyle = '#fff';

						context.shadowOffsetX = 3;
						context.shadowOffsetY = 3;
						context.shadowBlur = 4;
						context.shadowColor = 'rgba(0, 0, 0, 0.8)';

						context.fillText(text, width - metric.width - 8, height - 8);

						context.restore();
					}
				});
			});

		</script>

		<?php }?>
</head>

<body <?php body_class(); ?>>
	<header id="header">
	    <div id="header-content">
	        <div id="social-logos">
		        <img width="22" height="22" src="<?php bloginfo('template_directory'); ?>/images/facebook-logo.png" />
		         <img width="22" height="22" src="<?php bloginfo('template_directory'); ?>/images/instagram-logo.png" />
		        <img width="22" height="22" src="<?php bloginfo('template_directory'); ?>/images/pinterest-logo.png" />
		    </div>
		    <div id="header-title">
		       <h1>JO TAYLOR</h1>
		    </div>

		</div>
	</header>

	<div id="nav-m">
		<nav id="access" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'depth' => 1 ) ); ?><!--<img width="29" height="29" src="<?php bloginfo('template_directory'); ?>/images/FB-f-Logo__blue_29.png" />-->
		</nav><!-- #access -->
	</div>
 <?php if (is_front_page()  ) {?>
	<div id="homecanvas" style="width:980px;height:500px;margin:0 auto;border:1px solid #FFFFFF;">
<canvas id="kenburns" width="980" height="500">
				<p>Your browser is out of date and is unable to support some of the technologies available on this website. Please upgrade to the latest version of Internet Explorer <a href="http://windows.microsoft.com/en-GB/internet-explorer/download-ie">http://windows.microsoft.com/en-GB/internet-explorer/download-ie</a> or use a different browser.</p>
			</canvas></div>
			<?php }?>
