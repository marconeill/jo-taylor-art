<?php
/**
 * Template Name: gallery
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_header(); ?>


	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<div id="content-m"><article class="post" id="post-<?php the_ID(); ?>">

	

			<h1></h1>
			
			<?php if (is_page(7) || $post->post_parent=="7") { ?>

<ul class="sub-item-menu"><?php wp_list_pages(array('depth' => -1, 'child_of' => 7, 'title_li' => __('Galleries'), 'sort_column'  => 'menu_order',  )); ?></ul> 

<?php } else { ?>
 
<?php } ?>	

			<!--<?php include (TEMPLATEPATH . '/_/inc/meta.php' ); ?>-->

			<div class="entry">

				<?php the_content(); ?>

				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>

			</div>

			<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

		</article></div>
		


		<?php endwhile; endif; ?>


<?php get_footer(); ?>
