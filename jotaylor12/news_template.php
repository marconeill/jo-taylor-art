<?php
/**
 * Template Name: News
 *
 * Selectable from a dropdown menu on the edit page screen.
 */
?>

<?php get_header(); ?>

<div id="content-m">

<?php
$page_id = 11; // substitute page_id of page you want content included from for "2"
$page = get_post($page_id);
echo "<h1>$page->post_title</h1>";
?>

<?php
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => 100,
  'post_type' => 'news',
  'order' => 'DESC',
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post(); ?>
<!-- Individual Address Block Starts -->
<div id="news-block">	
	<h2><?php the_title(); ?></h2>
	<?php the_content(); ?>
	<p style="clear:both"><a href="http://www.facebook.com/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank"></p>
Share this post on Facebook</a> | <a href="mailto:?subject=Take a look at this post on Jo Taylor Art - <?php echo get_permalink(); ?>">Email to a friend</a>
	</div>


    <?php
  endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>
            </div><!-- #content -->


<?php get_footer(); ?>