<footer id="footer" class="source-org vcard copyright">

	<p class="owner-l">&copy; Jo Taylor Limited&trade; | All Rights Reserved<br>

<a href="https://www.facebook.com/jo.taylor.7161953" target="_blank"><img width="70" src="<?php bloginfo('template_directory'); ?>/images/like-button-2015-06.png" /></a>

	</p>


	<p class="owner-r"><a href="http://www.marconeill.co.uk">Website by Marc O'Neill</a></p>

</footer>

<!-- here comes the javascript -->
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
<!-- jQuery is called via the Wordpress-friendly way via functions.php -->

<!-- this is where we put our custom functions -->
<script src="<?php bloginfo('template_directory'); ?>/_/js/functions.js"></script>



</body>

</html>
